from glob import glob
from setuptools import setup
import cmake_support as cmake

name = 'pycall'

bin_module_names = ['core', 'ipc_streams']
cmake_modules = [cmake.CMakeExtension('%s.%s'%(name, module), 'bindings') for module in bin_module_names]

packages = [name] + ['%s.%s'%(name, module) for module in bin_module_names]

setup(
    name=name,
    version='@CMAKE_PROJECT_VERSION@',
    author='@AUTHOR_NAME@',
    author_email='@AUTHOR_MAIL@',
    description='A package for listening to C++ processes allowing them to remotely call python functions',
    ext_modules=cmake_modules,
    cmdclass=dict(build_ext=cmake.CMakeBuild),
    zip_safe=False,
    packages=packages,
    install_requires=[
          'numpy',
    ]
)
