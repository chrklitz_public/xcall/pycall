#include <iostream>

using namespace std;

#include <pycall/core/pycaller.hpp>
#include <pycall/core/pycall.hpp>
#include <pycall/ipc_streams/interprocess_stream.h>
#include <pycall/core/typestring_tree.hpp>

#include <vector>
#include <map>


/*
namespace pycall {
namespace detail {


template<typename T>
struct from { };

struct to_base { };
template<typename T>
struct to : public to_base { };

struct caster_base { };
template<typename T>
struct caster : public caster_base { };

struct template_args_base { };
template<typename... Args>
struct template_args : public template_args_base
{
  using tlist = pycall::typelist<Args...>;
};



template<typename T>
struct extract_template_type //lets call it extract_value_type
{
    using type = T;
};
template<template<typename> class X, typename T>
struct extract_template_type<X<T>>   //specialization
{
    using type = T;
};



template<typename Base, typename T, typename... Args>
struct extract_type_from_varargs {
  using wrapped_type = typename std::conditional<std::is_base_of<Base, T>::value, T, typename extract_type_from_varargs<Base, Args...>::wrapped_type>::type;
};
template<typename Base, typename T>
struct extract_type_from_varargs<Base, T> {
  using wrapped_type = typename std::conditional<std::is_base_of<Base, T>::value, T, pycall::type<pycall::none>>::type;
};

template<typename... Args>
using get_caster_from_varargs = typename extract_template_type<typename extract_type_from_varargs<caster_base, Args...>::wrapped_type>::type;

template<typename... Args>
using get_to_t_from_varargs = typename extract_template_type<typename extract_type_from_varargs<to_base, Args...>::wrapped_type>::type;

template<typename... Args>
using get_templ_types_from_varargs = typename extract_type_from_varargs<template_args_base, Args...>::wrapped_type;

template<bool, typename tlist_temp>
struct tlist_help
{
  using type = pycall::typelist<>;
};
template<typename tlist_temp>
struct tlist_help<false, tlist_temp>
{
  using type = typename tlist_temp::tlist;
};
} // end namespace test
} // end namespace pycall
*/


namespace pycall {
namespace detail {
namespace connection_help {

struct from_base { };
template<typename T>
struct from : public from_base { using type = pycall::type<T>; };

struct to_base { };
template<typename T>
struct to : public to_base { using type = pycall::type<T>; };

struct caster_base { };
template<typename T>
struct caster : public caster_base { using type = T; };

struct template_args_base { };
template<typename... Args>
struct template_args : public template_args_base { using type = pycall::typelist<Args...>; };

struct term_type_base { };
template<typename T>
struct term_type : public term_type_base { using type = pycall::type<T>; };


template<typename Base, typename T, typename... Args>
struct find_type {
  using type = typename std::conditional<std::is_base_of<Base, T>::value, typename T::type, typename find_type<Base, Args...>::type>::type;
};
template<typename Base, typename T>
struct find_type<Base, T> {
  using type = typename std::conditional<std::is_base_of<Base, T>::value, typename T::type, pycall::none>::type;
};

template<typename Ref>
bool find_object(Ref &ref) {
  (void) ref;
  return false;
}
template<typename Ref, typename... Ts>
bool find_object(Ref &ref, Ref current, Ts...) {
  ref = current;
  return true;
}
template<typename Ref, typename T, typename... Ts>
bool find_object(Ref &ref, T current, Ts... rest) {
  (void) current;
  return find_object<Ref>(ref, rest...);
}



template<typename... Args>
struct connection_info
{
private:
  template<typename Base>
  using find_type = find_type<Base, Args...>;

  using from_t = typename find_type<from_base>::type;
  using to_t = typename find_type<to_base>::type;
  static_assert(!std::is_same<pycall::none, from_t>::value, "PYCALL_FROM is not optional in PYCALL_CONNECT");
  static_assert(!std::is_same<pycall::none, to_t>::value, "PYCALL_TO is not optional in PYCALL_CONNECT");

  using caster_t_temp = typename find_type<caster_base>::type;
  using tlist_t_temp = typename find_type<template_args_base>::type;
  using caster_t = typename std::conditional<std::is_same<pycall::none, caster_t_temp>::value, pycall::implicit_caster, caster_t_temp>::type;
  using tlist_t = typename std::conditional<std::is_same<pycall::none, tlist_t_temp>::value, pycall::typelist<>, tlist_t_temp>::type;
  static_assert(!std::is_same<pycall::none, caster_t>::value, "optional caster is pycall::none which should not happen");
  static_assert(!std::is_same<pycall::none, tlist_t>::value, "optional template-list is pycall::none which should not happen");

public:
  from_t from;
  to_t to;
  caster_t caster;
  tlist_t tlist;
};

template<typename... Args>
struct terminal_info
{
private:
  template<typename Base>
  using find_type = find_type<Base, Args...>;
  using term_t = typename find_type<term_type_base>::type;
  using typestr_t = typename find_type<pycall::detail::type_string_t>::type;
  static_assert(!std::is_same<pycall::none, typestr_t>::value, "PYCALL_TYPE_STR is not optional in a terminal connection");
  static_assert(!std::is_same<pycall::none, term_t>::value, "PYCALL_TERMINAL_TYPE is not optional in a terminal connection");

public:
  terminal_info(Args... args)
  {
    find_object<pycall::detail::type_string_t>(typestr, args...);
  }

  term_t term;
  pycall::detail::type_string_t typestr; // select object from constructor args via a function similar to find_type
};


template<typename to_t, typename caster_t, typename tl>
struct connection_return_triple {
  connection_return_triple(to_t to, caster_t c, tl t) : to(to), caster(c), tlist(t) { }
  connection_return_triple(to_t to) : to(to) { }
  connection_return_triple() = default;

  to_t to;
  caster_t caster;
  tl tlist;
};

template<typename... Args>
connection_info<Args...> connection_from_args(Args ...args) { return connection_info<Args...>{args...}; }

template<typename... Args>
terminal_info<Args...> terminal_from_args(Args ...args) { return terminal_info<Args...>{args...}; }

} // end namespace connection_help
} // end namespace detail
} // end namespace pycall





#define PYCALL_FROM(...) pycall::detail::connection_help::from<__VA_ARGS__>()
#define PYCALL_TO(...) pycall::detail::connection_help::to<__VA_ARGS__>()
#define PYCALL_CASTER(...) pycall::detail::connection_help::caster<__VA_ARGS__>()
#define PYCALL_TEMPLATE_ARGS(...) pycall::detail::connection_help::template_args<__VA_ARGS__>()

#define PYCALL_TERMINAL_TYPE(...) pycall::detail::connection_help::term_type<__VA_ARGS__>()
#define PYCALL_TYPE_STR(...) pycall::detail::type_string_t{__VA_ARGS__}


#define PYCALL_CONNECT__(...) \
  static auto connection( \
    decltype(pycall::detail::connection_help::connection_from_args(__VA_ARGS__).from), \
    decltype(pycall::detail::connection_help::connection_from_args(__VA_ARGS__)) *info = nullptr \
  ) -> pycall::detail::connection_help::connection_return_triple<decltype(info->to), decltype(info->caster), decltype(info->tlist)> \
  { \
    return pycall::detail::connection_help::connection_return_triple<decltype(info->to), decltype(info->caster), decltype(info->tlist)>();  \
  }

#define PYCALL_TERMINAL__(...) \
  static pycall::detail::connection_help::connection_return_triple<pycall::detail::type_string_t, pycall::none, pycall::typelist<>> \
  connection(decltype(pycall::detail::connection_help::terminal_from_args(__VA_ARGS__).term), \
  decltype(pycall::detail::connection_help::terminal_from_args(__VA_ARGS__)) info = pycall::detail::connection_help::terminal_from_args(__VA_ARGS__) \
  ) { \
    return pycall::detail::connection_help::connection_return_triple<pycall::detail::type_string_t, pycall::none, pycall::typelist<>>(info.typestr); \
  }


struct MyMapping
{
  PYCALL_CONNECT__(PYCALL_FROM(std::string), PYCALL_TO(int64_t), PYCALL_CASTER(pycall::implicit_caster))
  PYCALL_CONNECT__(PYCALL_FROM(std::map<int, float>), PYCALL_TO(int16_t))

  PYCALL_TERMINAL__(PYCALL_TERMINAL_TYPE(int16_t), PYCALL_TYPE_STR("int"))
};



int main()
{
  /*
  static_assert(std::is_same<decltype(MyMapping::connection(pycall::type<std::map<int, float>>()).caster), pycall::implicit_caster>::value, "");
  pycall::detail::type_string_t{"e"};
  std::cout << MyMapping::connection(pycall::type<int16_t>()).to.type_string << std::endl;
  */
  // auto res = MyMapping::connect(pycall::type<COOL>());
  // std::cout << res.second;
  /*
  interprocess_stream stream;
  std::shared_ptr<pycall::tuple_wrapper> c = pycall::cast_to_terminal<pycall::default_mapping>(std::make_tuple<int, std::string>(-10, "Hello"));
  stream << c;

  std::tuple<int, std::string> v = pycall::cast_from_terminal<pycall::default_mapping, std::tuple<int, std::string>>(std::move(c));
  */


  // pycall::cast_to_terminal<pycall::default_mapping>(std::list<int>({1, 2, 3}))->write_to_stream(stream);



  pycall::pycaller<pycall::default_mapping, interprocess_stream> caller = pycall::pycaller<pycall::default_mapping, interprocess_stream>::from_stream<>();

  using ret_t = std::vector<std::tuple<int, float, std::vector<int16_t>>>;
  std::pair<ret_t, bool> res1 = caller.call<ret_t>("myfunc", std::vector<std::tuple<std::string, int, float>>({std::make_tuple<std::string, int, float>("test", 42, 13.04f)}), std::string("Hello from C++"));
  std::cout << "[";
  for (const auto &v : res1.first) {
    std::cout << "(";
    std::cout << std::get<0>(v) << ", " << std::get<1>(v) << ", ";
    std::cout << "[";
    for (const auto &e : std::get<2>(v)) {
      std::cout << e << ", ";
    }
    std::cout << "]";
    std::cout << "), ";
  }
  std::cout << "]" << std::endl;

  // std::cout << ((int) res1.first) << std::endl;

  // std::pair<pycall::none, bool> res2 = caller.call<pycall::none>("numpy.zerosadd", std::string("nice"), 7);
  // std::cout << res2 << std::endl;


  return 0;
}
