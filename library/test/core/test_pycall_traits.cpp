#include <gtest/gtest.h>

#include "mock_connections.hpp"


TEST(PYCALL_CASTER_TRAITS, CASTER_TYPE) {
  ASSERT_TRUE((std::is_same<pycall::implicit_caster, typename pycall::caster_type<test_mapping, int8_t>::type>::value));
  ASSERT_TRUE((std::is_same<test_caster1, typename pycall::caster_type<test_mapping, uint8_t>::type>::value));
}

TEST(PYCALL_CASTER_TRAITS, HAS_CUSTOM_DOWN_CAST) {
  ASSERT_TRUE((!pycall::has_custom_downcast<test_mapping, int8_t>::value));
  ASSERT_TRUE((!pycall::has_custom_downcast<test_mapping, uint8_t>::value));
  ASSERT_TRUE((!!pycall::has_custom_downcast<test_mapping, uint16_t>::value));
  ASSERT_TRUE((!!pycall::has_custom_downcast<test_mapping, std::vector<int>>::value));
}

TEST(PYCALL_CASTER_TRAITS, HAS_CUSTOM_UP_CAST) {
  ASSERT_TRUE((!pycall::has_custom_upcast<test_mapping, int8_t>::value));
  ASSERT_TRUE((!!pycall::has_custom_upcast<test_mapping, uint8_t>::value));
  ASSERT_TRUE((!pycall::has_custom_upcast<test_mapping, uint16_t>::value));
  ASSERT_TRUE((!!pycall::has_custom_upcast<test_mapping, std::vector<float>>::value));
}

TEST(PYCALL_CASTER_TRAITS, TERMINAL_TYPE) {
  ASSERT_TRUE((std::is_same<int64_t, typename pycall::terminal_type<test_mapping, int8_t>::type>::value));
  ASSERT_TRUE((std::is_same<float, typename pycall::terminal_type<test_mapping, std::string>::type>::value));
  ASSERT_TRUE((!std::is_same<int32_t, typename pycall::terminal_type<test_mapping, int32_t>::type>::value));

  // Test template connections including a comma seperated template parameter list (for std::map)
  ASSERT_TRUE((std::is_same<int64_t, typename pycall::terminal_type<test_mapping, std::map<int, float>>::type>::value));
  ASSERT_TRUE((std::is_same<int64_t, typename pycall::terminal_type<test_mapping, std::vector<float>>::type>::value));
}

TEST(PYCALL_CASTER_TRAITS, UPCAST) {
  auto casted1 = pycall::upcast<test_mapping>(TestB());
  ASSERT_TRUE((std::is_same<TestA, decltype(casted1)>::value));

  auto casted2 = pycall::upcast<test_mapping>(static_cast<int16_t>(0));
  ASSERT_TRUE((std::is_same<int32_t, decltype(casted2)>::value));

  auto casted3 = pycall::upcast<test_mapping>(static_cast<int32_t>(0));
  ASSERT_TRUE((std::is_same<int64_t, decltype(casted3)>::value));

  auto casted4 = pycall::upcast<test_mapping>(static_cast<uint16_t>(0));
  ASSERT_TRUE((std::is_same<int16_t, decltype(casted4)>::value));
}

TEST(PYCALL_CASTER_TRAITS, DOWNCAST) {
  auto casted1 = pycall::downcast<test_mapping, std::string>(static_cast<float>(1.4f));
  ASSERT_TRUE((std::is_same<std::string, decltype(casted1)>::value));
  ASSERT_EQ(casted1, std::to_string(1.4f));

  TestA a;
  a.i = 3.4f;
  auto casted2 = pycall::downcast<test_mapping, TestB>(a);
  ASSERT_TRUE((std::is_same<TestB, decltype(casted2)>::value));
  ASSERT_FLOAT_EQ(casted2.i, 4.4f);
}

TEST(PYCALL_CASTER_TRAITS, CAST_TO_TERMINAL) {
  auto casted1 = pycall::cast_to_terminal<test_mapping>(static_cast<int8_t>(0));
  ASSERT_TRUE((std::is_same<int64_t, decltype(casted1)>::value));

  auto casted2 = pycall::cast_to_terminal<test_mapping>(TestB());
  ASSERT_TRUE((std::is_same<float, decltype(casted2)>::value));
  ASSERT_FLOAT_EQ(casted2, 13.0);

  auto casted3 = pycall::cast_to_terminal<test_mapping>(static_cast<float>(0.0));
  ASSERT_TRUE((std::is_same<float, decltype(casted3)>::value));
}


TEST(PYCALL_CASTER_TRAITS, CAST_FROM_TERMINAL) {
  auto casted1 = pycall::cast_from_terminal<test_mapping, TestA>(static_cast<float>(1.0f));
  ASSERT_TRUE((std::is_same<TestA, decltype(casted1)>::value));
  ASSERT_FLOAT_EQ((casted1.i), 3.0);

  auto casted2 = pycall::cast_from_terminal<test_mapping, int64_t>(static_cast<int64_t>(0));
  ASSERT_TRUE((std::is_same<int64_t, decltype(casted2)>::value));

  auto casted3 = pycall::cast_from_terminal<test_mapping, TestB>(static_cast<float>(1.0f));
  ASSERT_TRUE((std::is_same<TestB, decltype(casted3)>::value));
  ASSERT_FLOAT_EQ(casted3.i, 4.0f);

  auto casted4 = pycall::cast_from_terminal<test_mapping, std::string>(static_cast<float>(10.0f));
  ASSERT_TRUE((std::is_same<std::string, decltype(casted4)>::value));
  ASSERT_EQ(casted4, std::to_string(10.0f));

  auto casted5 = pycall::cast_from_terminal<test_mapping, float>(static_cast<float>(3.14f));
  ASSERT_TRUE((std::is_same<float, decltype(casted5)>::value));
  ASSERT_FLOAT_EQ(casted5, 3.14f);
}





TEST(PYCALL_CONNECTION_TRAITS, IS_TERMINAL) {
  ASSERT_TRUE((pycall::is_terminal<test_mapping, float>::value));
  ASSERT_TRUE((pycall::is_terminal<test_mapping, int64_t>::value));

  ASSERT_TRUE((!pycall::is_terminal<test_mapping, TestA>::value));
  ASSERT_TRUE((!pycall::is_terminal<test_mapping, TestB>::value));
  ASSERT_TRUE((!pycall::is_terminal<test_mapping, int32_t>::value));
}

TEST (PYCALL_CONNECTION_TRAITS, CONNECTED_TYPE) {
  ASSERT_TRUE((std::is_same<float, typename pycall::connected_type<test_mapping, TestA>::type>::value));
  ASSERT_TRUE((std::is_same<TestA, typename pycall::connected_type<test_mapping, TestB>::type>::value));
  ASSERT_TRUE(!(std::is_same<int8_t, typename pycall::connected_type<test_mapping, uint16_t>::type>::value));
}

TEST (PYCALL_CONNECTION_TRAITS, TYPE_STRING) {
  ASSERT_TRUE((pycall::type_string<test_mapping, float>()) == "float");
  ASSERT_TRUE((pycall::type_string<test_mapping, std::string>()) == "float");
  ASSERT_TRUE((pycall::type_string<test_mapping, TestB>()) == "float");

  ASSERT_TRUE((pycall::type_string<test_mapping, int8_t>()) == "int");
  ASSERT_TRUE((pycall::type_string<test_mapping, uint8_t>()) == "int");
  ASSERT_TRUE((pycall::type_string<test_mapping, int32_t>()) == "int");
}

