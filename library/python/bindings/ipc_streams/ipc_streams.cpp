#include <pybind11/pybind11.h>
namespace py = pybind11;

void bind_interprocessing_stream(py::module &m);

PYBIND11_MODULE(_ipc_streams, m) {
  bind_interprocessing_stream(m);
}
