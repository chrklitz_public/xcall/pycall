/**
 * \file pycall.hpp
 * \brief Includes the different pycall prefixed headers. Furthermore it also provides a
 * default type-map pycall::default_mapping which maps the most common types in the
 * c++ standard template library and the pycall::stream interface together with some standard
 * stream-write/read overloads for it.
 */

/*!
 * \mainpage Python remote procedure call from C++
 *
 * \section module_sec Modules
 *
 * - \ref pycall/core <br>
 *    The core module provides the essential api for calling python functions. It is just a framework with little to no
 *    fully implemented stuff. It makes heavy use of templates and is designed with the idea of modularity and the plugin of features.
 *
 * - \ref pycall/ipc_streams <br>
 *    This module contains implementations of different interprocess communication mechanisms compatible to the pycall::pycaller api.
 *    - interprocess_stream (shared memory based)
 *    - tcp_stream (tcp based using the internet; NOT IMPLEMENTED YET)
 *
 * \section pycompile_sec Create a pycall python package
 * -# Install pycall
 * -# Inside the installation directory goto the folder pycall_python
 * -# Run with the desired python version: python setup.py bdist_wheel
 * -# The dist folder now contains a pip-installable python package
 */
#ifndef PYCALL_CPP_H
#define PYCALL_CPP_H


#include <inttypes.h>
#include <string>
#include <vector>
#include <list>
#include <memory>

#include "pycall_traits.hpp"
#include "pycall_mappings.hpp"
#include "pycall_stream.hpp"


#endif // PYCALL_CPP_H
