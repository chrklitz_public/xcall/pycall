#ifndef MOCK_CONNECTIONS_H
#define MOCK_CONNECTIONS_H

#include <pycall/core/pycall.hpp>
#include <vector>
#include <map>

struct test_caster1
{
  static void upcast(uint8_t from, int8_t &to) {
    to = from;
  }
};

struct test_caster2 {
  static void downcast(int16_t from, uint16_t &to) {
    to = from;
  }
};

struct test_caster3 {
  static void upcast(std::string from, float &to) {
    to = from.length();
  }

  static void downcast(float from, std::string &to) {
    to = std::to_string(from);
  }
};

struct test_caster4 {
  template<typename T>
  static void upcast(std::vector<T> from, int8_t &to) {
    to = from.size();
  }
  template<typename T>
  static void downcast(int8_t from, std::vector<T> &to) {
    to = {};
  }
};

struct test_caster5 {
  template<typename T, typename Q>
  static void upcast(std::map<T, Q> from, int64_t &to) {
    to = from.size();
  }
  template<typename T, typename Q>
  static void downcast(int64_t from, std::map<T, Q> &to) {
    to = {};
  }
};

class TestB;

class TestA {
public:
  TestA() = default;
  TestA(const TestB &) { }
  TestA(float k) : i(k + 2.0) { }

  // the const is important for the compiler to perform the static_cast
  operator float() const { return 13.0; }

  float i;
};

class TestB {
public:
  TestB() = default;
  TestB(const TestA &a) : i(a.i + 1.0) { }

  float i;
};


struct test_mapping
{
  PYCALL_CONNECT(int8_t, int16_t)
  PYCALL_CONNECT(int16_t, int32_t)
  PYCALL_CONNECT(int32_t, int64_t)
  PYCALL_TERMINAL(int64_t, "int")

  PYCALL_CONNECT(uint8_t, int8_t, test_caster1)
  PYCALL_CONNECT(uint16_t, int16_t, test_caster2)
  PYCALL_CONNECT(uint64_t, uint16_t)

  PYCALL_TERMINAL(float, "float")
  PYCALL_CONNECT(std::string, float, test_caster3)

  PYCALL_CONNECT(TestA, float)
  PYCALL_CONNECT(TestB, TestA)

  template<typename T>
  PYCALL_CONNECT(std::vector<T>, int8_t, test_caster4)

  template<typename T, typename Q>
  PYCALL_CONNECT(std::map<T, Q>, int64_t, test_caster5)

  PYCALL_TERMINAL(double, "double")
  PYCALL_CONNECT(uint32_t, double)
};



#endif // MOCK_CONNECTIONS_H
