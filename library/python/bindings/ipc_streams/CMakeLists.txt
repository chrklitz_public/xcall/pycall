set(SOURCE_FILES
    ipc_streams.cpp
    stream_class.cpp
)


if (USE_PYCALL_PYTHON)
    pybind11_add_module(pycall_python_ipc_stream ${SOURCE_FILES})
    target_link_libraries(pycall_python_ipc_stream PRIVATE ipc_streams)

    # auto rename built python module file
    set_target_properties(pycall_python_ipc_stream PROPERTIES OUTPUT_NAME "_ipc_streams")
else()
    add_custom_target(pycall_python_ipc_stream SOURCES ${SOURCE_FILES})
endif()


