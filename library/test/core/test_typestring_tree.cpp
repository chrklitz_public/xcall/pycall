#include <gtest/gtest.h>

#include <pycall/core/pycall.hpp>

TEST(TYPESTRING_TREE, GENERAL) {
  pycall::typestring_tree t;
  auto *child1 = &t.push_back("n1");
  auto &child2 = t.push_back("n2");
  child2.push_back("n3");
  ASSERT_TRUE((t.type_strings() == std::vector<std::string>{"n1", "n2"}));

  ASSERT_TRUE((child1 == &t.entries()[0].second));
}

TEST(TYPESTRING_TREE, BUILD_FROM_TYPE) {
  pycall::typestring_tree t1_1 = pycall::build_typestring_tree<pycall::default_mapping, int>();
  pycall::typestring_tree t1_2;
  ASSERT_TRUE((t1_1 == t1_2)) << "Expect equality: " << std::endl
                              << t1_1 << std::endl
                              << t1_2;

  pycall::typestring_tree t2_1 = pycall::build_typestring_tree<pycall::default_mapping, std::vector<int>>();
  pycall::typestring_tree t2_2;
  t2_2.push_back(pycall::type_string<pycall::default_mapping, int>());
  ASSERT_TRUE((t2_1 == t2_2)) << "Expect equality: " << std::endl
                              << t2_1 << std::endl
                              << t2_2;

  pycall::typestring_tree t3_1 = pycall::build_typestring_tree<pycall::default_mapping, std::vector<std::list<float>>>();
  pycall::typestring_tree t3_2;
  auto &child3_2 = t3_2.push_back(pycall::type_string<pycall::default_mapping, std::list<float>>());
  child3_2.push_back(pycall::type_string<pycall::default_mapping, float>());
  ASSERT_TRUE((t3_1 == t3_2)) << "Expect equality: " << std::endl
                              << t3_1 << std::endl
                              << t3_2;
}
