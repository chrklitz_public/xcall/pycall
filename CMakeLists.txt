cmake_minimum_required(VERSION 3.5)

project(pycall LANGUAGES CXX VERSION 0.0.1)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_INSTALL_PREFIX ${CMAKE_CURRENT_SOURCE_DIR}/../pycall_install)

option(USE_PYCALL_PYTHON "Build pycall into a binary python package" ON)
option(USE_PYCALL_CPP "Build the c++ part of this library" ON)
option(USE_PYCALL_TESTS "Build pycall tests module" ON)

# uses those options
add_subdirectory(library)

