#include <pybind11/numpy.h>

#include <iostream>

#include <pycall/core/binding_definitions.h>
#include <pycall/core/typestring_tree.hpp>
#include <list>
#include "pyreceiver.h"

py::object locate_function(const std::string &fname) {
  py::object fmodule = py::none(); // module of the fname prefix
  std::string fname_pure; // name of function without module prefix

  if (fname.find('.') != std::string::npos) {
    size_t dot_pos = fname.find_last_of('.');
    std::string fmodule_name = fname.substr(0, dot_pos);
    py::object import_module = py::module::import("importlib").attr("import_module");
    fmodule = import_module(fmodule_name);

    fname_pure = fname.substr(dot_pos + 1, fname.length());
  } else {
    py::module sys = py::module::import("sys");
    fmodule = sys.attr("modules")["__main__"];
    fname_pure = fname;
  }

  if (py::hasattr(fmodule, fname_pure.c_str())) {
    py::object pyfunc = fmodule.attr(fname_pure.c_str());
    if (py::hasattr(pyfunc, "__call__"))
      return pyfunc;
    else
      return py::none();
  }
  return py::none();
}

void bind_pyreceiver(pybind11::module &m) {
  py::class_<pyreceiver>(m, "pyreceiver")
      .def(py::init<pycall::stream &, stream_module &>())
      .def(py::init<pycall::stream &>())
      .def("listen", &pyreceiver::listen)
      ;
}

pyreceiver::pyreceiver(pycall::stream &stream, stream_module &sm)
  : stream(stream), sm(sm)
{

}

pyreceiver::pyreceiver(pycall::stream &stream)
  : stream(stream), sm(modules::default_comm)
{

}

void pyreceiver::listen() {
  while (true) {
    unmark_error(); // reset error information

    stream.receive();
    std::string cmd;
    stream >> cmd;

    if (cmd == CMD_CALL) {
      // Read some important infos from the stream:
      std::string argc_str;
      stream >> argc_str;
      int argc = std::atoi(argc_str.c_str());

      std::string fname;
      stream >> fname;

      std::string rtype_str;
      pycall::typestring_tree ts_tree;
      stream >> rtype_str >> ts_tree;
      std::cout << ts_tree << std::endl;

      std::cout << "argc: " << argc << std::endl;
      std::cout << "fname: " << fname<< std::endl;
      std::cout << "ret: " << rtype_str << std::endl;

      // Read arguments from stream:
      py::list args;
      for (size_t i = 0; i < argc; ++i) {
        std::string arg_type_str;
        stream >> arg_type_str;

        auto *read_arg = sm.find_stream_read(arg_type_str);
        if (read_arg != nullptr) {
          pycall::typestring_tree arg_ts_tree;
          stream >> arg_ts_tree;
          py::object arg = (*read_arg)(stream, sm, arg_ts_tree);
          py::print(arg);
          args.append(arg);
        } else {
          mark_error("Missing stream-read function for type_string \'" + arg_type_str + "\'");
          break;
        }
      }

      if (error()) // breaks the while-loop if an error occured in the above for-loop
        continue;

      stream_module::stream_write_func *write_return = nullptr;
      // for a void return this is not necessary
      if (rtype_str != VOID_TYPE_STRING) {
        write_return = sm.find_stream_write(rtype_str);
        if (write_return == nullptr) {
          mark_error("Missing stream-write function for type_string \'" + rtype_str + "\'");
          continue;
        }
      }

      py::object result = py::none();

      // call python function with arguments
      py::object pyfunc = locate_function(fname);
      if (!pyfunc.is_none()) {
        try {
          result = pyfunc(*args);
        } catch (py::error_already_set &err) {
          mark_error(py::str(err.value()).cast<std::string>());
          continue;
        }
      } else {
        mark_error("Function '" + fname + "\' not found");
        continue;
      }

      if (rtype_str == VOID_TYPE_STRING) {
        if (result.is_none()) {
          stream << std::string(CMD_RETURN_VOID);
        } else {
          mark_error("C++ call expected void return but got something else");
          continue;
        }
      } else {
        // write python function result to stream
        if (result.is_none()) {
          mark_error("C++ call expected non-void return but got python None");
          continue;
        }

        stream << std::string(CMD_RETURN);

        try {
          (*write_return)(stream, result, sm, ts_tree); // only called for non-void return
        } catch (const std::exception &e) {
          std::cout << "caught exception" << std::endl;
          stream.clear_send_buffer();
          mark_error(std::string(e.what()));
          continue;
        }
      }

      // In case all went well
      stream.send(); // return result to caller
      // the receive call in the next loop iteration makes sure
      // that the returned value can be recevied by the caller.
      // -> reduces the synchronization overhead

    } else if (cmd == CMD_STOP) {
      stream.send(); // hand-shake signal to the caller
      break;
    } else { // no matching command
      mark_error("Invalid command: " + cmd);
      continue;
    }
  } // end while loop

  std::cout << "end listen event loop" << std::endl;
}

bool pyreceiver::error()
{
  return error_occured;
}

std::string pyreceiver::error_description()
{
  return error_msg;
}


void pyreceiver::mark_error(const std::string &msg)
{
  error_occured = true;
  error_msg = msg;
  py::print("pyreceiver.listen() error: \'" + msg + "\'");

  stream << std::string(CMD_ERROR) << error_description();
  stream.send(); // signal error to caller
}

void pyreceiver::unmark_error()
{
  error_occured = false;
  error_msg = "";
}
