#include "interprocess_stream.h"

#include <QSharedMemory>
#include <QCoreApplication>
#include <cstring>

#include <iostream>

constexpr int header_size = sizeof(size_t); // size of header in each row
constexpr int tag_size = sizeof(interprocess_stream::tagsize_t); // size of the size tag before the actual data in a row
uint64_t interprocess_stream::key_cnt = 0;

interprocess_stream::interprocess_stream()
  : mp(IPS_MIN_COMM_CHANNEL_SIZE)
{
  std::cout << "mp1 constructed: " << this->key() << std::endl;
}

interprocess_stream::interprocess_stream(const std::string &key)
  : mp(key)
{
  std::cout << "mp2 attached: " << this->key() << std::endl;
}

std::string interprocess_stream::key() const
{
  return mp.key();
}

bool interprocess_stream::read_raw(void **data, size_t *size)
{
  void *tag_ptr;
  if (!read_raw_unchecked(&tag_ptr, tag_size))
    return false;

  tagsize_t tag = *static_cast<tagsize_t *>(tag_ptr);
  *size = static_cast<int>(tag);
  return read_raw_unchecked(data, *size);
}

bool interprocess_stream::add_raw(const void *data, size_t size)
{
  tagsize_t tag = static_cast<tagsize_t>(size);
  return add_raw_unchecked(&tag, tag_size) && add_raw_unchecked(data, size);
}


bool interprocess_stream::read_raw_unchecked(void **data, size_t size)
{
  void *row_data = rows[row_index]->data();
  size_t *bytes_size = static_cast<size_t *>(row_data);

  if (col_index + size > *bytes_size) {
    next_row();
    row_data = rows[row_index]->data(); // next_row updates row_index
    bytes_size = static_cast<size_t *>(row_data);
  }

  *data = static_cast<void *>(static_cast<char *>(row_data) + header_size + col_index);
  col_index += size;
  return true;
}

bool interprocess_stream::add_raw_unchecked(const void *data, size_t size)
{
  // This process finished reading from stream and therefore the rows buffer can be re-used.
  if (!send_mode) { // was in receive mode
    clear(); // clear previously received stuff
    send_mode = true;
  }

  if (rows.size() == 0) {
    if (!new_row(size))
      return false;
  } else if (header_size + col_index + size > rows[row_index]->size()) {
    // jump to next row
    if (!new_row(size))
      return false;
    next_row();
  }

  void *row_data = rows[row_index]->data();

  size_t *bytes_size = static_cast<size_t *>(row_data);
  char *bytes = static_cast<char *>(row_data) + header_size;

  std::memcpy(bytes + col_index, data, size);
  *bytes_size += size;
  col_index += size;
  return true;
}

bool interprocess_stream::send()
{
  return mp.send();
}

bool interprocess_stream::receive()
{
  if (!mp.receive()) {
    return false;
  }

  // waits for other process to call send before resource deallocation.
  // This makes sure that the other process definitly does not need the resources (rows) anymore.
  if (send_mode) { // was in send mode
    clear();
    send_mode = false;
  }

  // read keys from mp and attach to the memory segments
  for (auto &key : mp.received_strings()) {
    auto nrow = std::shared_ptr<QSharedMemory>(new QSharedMemory(QString::fromStdString(key)));
    if (!nrow->attach()) {
      for (auto &row : rows) {
        row->detach();
      }
    } else {
      rows.push_back(nrow);
    }
  }
  return true;
}

void interprocess_stream::clear_send_buffer()
{
  clear();
}

std::string interprocess_stream::unique_row_key()
{
  return mp.key() + "_" +
      std::to_string(key_cnt++) +
      "_" +
      QString::number(QCoreApplication::applicationPid()).toStdString();
}

bool interprocess_stream::new_row(int data_size)
{
  std::string nkey = unique_row_key();

  auto row = std::shared_ptr<QSharedMemory>(new QSharedMemory(QString::fromStdString(nkey)));
  if (row->create(std::max(IPS_MIN_SEGMENT_SIZE, static_cast<int>(data_size)) + header_size)) {
    rows.push_back(row);
    mp.add_string(nkey);

    size_t *bytes_used = static_cast<size_t *>(row->data());
    *bytes_used = 0;
    return true;
  }
  std::cout << "error: " << row->errorString().toStdString() << std::endl;
  return false;
}

void interprocess_stream::next_row()
{
  row_index++;
  col_index = 0;
}

void interprocess_stream::clear()
{
  row_index = 0;
  col_index = 0;
  rows.clear(); // detach previously sent segments
}


