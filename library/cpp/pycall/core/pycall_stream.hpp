#ifndef PYCALL_STREAM_HPP
#define PYCALL_STREAM_HPP

#include <limits>


#if __BYTE_ORDER__ == __BIG_ENDIAN
#define PYCALL_BYTE_SWAP_2(val) val
#define PYCALL_BYTE_SWAP_4(val) val
#define PYCALL_BYTE_SWAP_8(val) val
#else
#define PYCALL_BYTE_SWAP_2(val) \
 ( (((val) >> 8) & 0x00FF) | (((val) << 8) & 0xFF00) )

// Swap 4 byte, 32 bit values:
#define PYCALL_BYTE_SWAP_4(val) \
 ( (((val) >> 24) & 0x000000FF) | (((val) >>  8) & 0x0000FF00) | \
   (((val) <<  8) & 0x00FF0000) | (((val) << 24) & 0xFF000000) )

// Swap 8 byte, 64 bit values:
#define PYCALL_BYTE_SWAP_8(val) \
 ( (((val) >> 56) & 0x00000000000000FF) | (((val) >> 40) & 0x000000000000FF00) | \
   (((val) >> 24) & 0x0000000000FF0000) | (((val) >>  8) & 0x00000000FF000000) | \
   (((val) <<  8) & 0x000000FF00000000) | (((val) << 24) & 0x0000FF0000000000) | \
   (((val) << 40) & 0x00FF000000000000) | (((val) << 56) & 0xFF00000000000000) )
#endif




namespace pycall {

template<typename T, typename std::enable_if<sizeof(T) == 8, bool>::type = true>
inline void big_endian_byte_swap(T &obj) {
  obj = PYCALL_BYTE_SWAP_8(obj);
}
template<typename T, typename std::enable_if<sizeof(T) == 4, bool>::type = true>
inline void big_endian_byte_swap(T &obj) {
  obj = PYCALL_BYTE_SWAP_4(obj);
}
template<typename T, typename std::enable_if<sizeof(T) == 2, bool>::type = true>
inline void big_endian_byte_swap(T &obj) {
  obj = PYCALL_BYTE_SWAP_2(obj);
}


/*!
 * \brief The stream class is the interface for all compatible interprocess communication.
 * If a class inherits from it and overrides its functions it can be used
 * in the pycaller api.
 */
class stream {
public:
  /*!
   * \brief read_raw reads the next segment of received data from the stream.
   *
   * Both stream::read_raw and stream::add_raw must not (but can) synchronize with each other. They can instead just
   * read/write from/to a local buffer. Instead, transfering data can be done by the stream::send and stream::receive
   * call in batches. Reading data from another process is only allowed after a receive call in this process
   * which got unblocked/committed by the corresponding send call in the other process.
   *
   * \param data will be assigned with a pointer to a segment of the read raw data.
   * \param size will contain the size of the read segment.
   * \return true to signify that both reading the data and assigning it was successful.
   */
  virtual bool read_raw(void **data, size_t *size) = 0;

  /*!
   * \brief add_raw adds a segment of data to the stream buffer.
   *
   * Both stream::read_raw and stream::add_raw must not (but can) synchronize with each other. They can instead just
   * read/write from/to a local buffer. Instead, transfering data can be done by the stream::send and stream::receive
   * call in batches. Added data should only be read-able after this process committed them using the stream::send call
   * and the other process received them using the stream::receive function.
   *
   * \param data contains the raw data which should be transfered.
   * \param size contains the size of the added raw data
   * \return true to signify that adding the data was successful.
   */
  virtual bool add_raw(const void *data, size_t size) = 0;

  /*!
   * \brief send is a synchronization mechanism with the corresponding instance in another process. It can also be used to send
   * added data from a local buffer in one batch to the other process. A call to this function marks the deadline for potential
   * asynchronous sharing of added data using the stream::add_raw function.
   *
   * Calling this function unblocks the other process' stream::receive call but is itself non-blocking.
   * This function does not wait for the other process to read the sent data. This means that the corresponding
   * interprocess communication mechanism is not allowd to be deleted (e.g. shared memory) until the next stream::receive call in this process.
   *
   * \return true if all went well, false otherwise
   */
  virtual bool send() = 0;

  /*!
   * \brief receive is a synchronization mechanism with the corresponding instance in another process. It can also be used to receive
   * added data in another process' local buffer in one batch. A call to this function marks a deadline for potential
   * asynchronous sharing of added data using the stream::add_raw function.
   *
   * Calling this function blocks until the stream::send function is called on the corresponding instance in the other process.
   *
   * \return true if all went well, false otherwise
   */
  virtual bool receive() = 0;

  /*!
   * \brief clear_send_buffer removes all the data added previously using stream::add_raw . The first stream::read_raw call by the other process must
   * read the content which gets added with the first stream::add_raw call in this process after calling this function.
   */
  virtual void clear_send_buffer() = 0;

  /*!
   * \brief key returns an indentifier string which can but must not be used to connect an instance of this class in another process
   * to the instance where this function gets called on.
   *
   * \return the identifier string (e.g. shared memory key for shared memory based interprocess communication)
   */
  virtual std::string key() const { return ""; }

  virtual ~stream() = default;
};

} // end namespace pycall




// ===================================================================
// ===================================================================
// ===================================================================
// =================== STREAM FUNCTION OVERLAODS =====================
// ===================================================================
// ===================================================================
// ===================================================================


inline pycall::stream &operator<<(pycall::stream &s, const std::string &obj) {
  s.add_raw(obj.data(), obj.length());
  return s;
}
inline pycall::stream &operator>>(pycall::stream &s, std::string &obj) {
  void *data;
  size_t len;
  s.read_raw(&data, &len);
  obj = std::string(static_cast<char *>(data), len);
  return s;
}


template<typename T, typename std::enable_if<std::is_integral<T>::value, bool>::type = true>
inline pycall::stream &operator<<(pycall::stream &s, T obj) {
  pycall::big_endian_byte_swap(obj); // to big-endian
  s.add_raw(&obj, sizeof(obj));
  return s;
}
template<typename T, typename std::enable_if<std::is_integral<T>::value, bool>::type = true>
inline pycall::stream &operator>>(pycall::stream &s, T &obj) {
  void *data;
  size_t len;
  s.read_raw(&data, &len);
  if (len != sizeof(obj))
    throw "Invalid size of stream argument";
  obj = *static_cast<T *>(data);
  pycall::big_endian_byte_swap(obj); // from big-endian
  return s;
}

inline pycall::stream &operator<<(pycall::stream &s, double obj) {
  static_assert(std::numeric_limits<double>::is_iec559, "Currently there is no support for non IEEE 754 double");
  static_assert(sizeof(double) == 8, "Currently there is no support for 64-bit double numbers");
  int64_t __obj = PYCALL_BYTE_SWAP_8(*reinterpret_cast<int64_t *>(&obj));
  s.add_raw(&__obj, sizeof(double));
  return s;
}

inline pycall::stream &operator>>(pycall::stream &s, double &obj) {
  void *data;
  size_t len;
  s.read_raw(&data, &len);
  if (len != sizeof(double))
    throw "Invalid size of stream argument";
  int64_t __obj = PYCALL_BYTE_SWAP_8(*static_cast<int64_t *>(data));
  obj = *reinterpret_cast<double *>(&__obj);
  return s;
}

#endif // PYCALL_STREAM_HPP
